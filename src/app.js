let oldPosX;
let oldPosY;

document.body.onload = function(){
    bodyWrapperInit();
}

function bodyWrapperInit(){
    var bodyW = document.querySelector("#bodyWrapper");
    
    oldPosX = null;
    oldPosY = null;

    bodyW.ontouchstart = (e) => {
        oldPosY = e.changedTouches[0]["clientY"];
    };
    bodyW.ontouchmove = function(e){
        let currPosY = e.changedTouches[0]["clientY"];
        var footer = document.querySelector("footer");
        if(currPosY<oldPosY){
            console.log("Subiendo");
            footer.style.bottom = "-60px";

        }else{
            console.log("Bajando");
            footer.style.bottom = "0px";
        }
        oldPosY = currPosY;
    };
}